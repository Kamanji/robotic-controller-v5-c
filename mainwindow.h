#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "ui_mainwindow.h"
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QDir>
#include <mainwindow/settingsWindow/settingsdialog.h>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

signals:

    void setCurrentSettings(std::map<QString,int> currentSettings);

public:



    void applySettings();

    void loadSettings();

    void saveSettings();

    std::string currentMode;

    settingsDialog * settingsdialog = new settingsDialog;

    std::vector<float> baseRotation;
    std::vector<float> baseBend;
    std::vector<float> mediumBend;
    std::vector<float> mediumRotation;
    std::vector<float> topBend;
    std::vector<float> topRotation;
    std::vector<float> gripper;

    void setCoordinatesSpinBox();

    bool animationIsActive = false;

    void emitRobot6dofCoordinates();
    void emitRobot4dofCoordinates();
    void emitRobot6dofAngles();
    void emitRobot4dofAngles();

    void setDefaultSettings();

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void writeToAnimation();

    void setSliders();

    void clearAnimation();

public slots:

    void slotSettingsMap(std::map<QString,int> settingsMap);

private slots:

    void on_action6dof_triggered();

    void on_action4dof_triggered();

    void on_actionScara_triggered();

    void on_sliderBaseRotation_valueChanged(int value);

    void on_sliderBaseBend_valueChanged(int value);

    void on_sliderMediumBend_valueChanged(int value);

    void on_sliderTopBend_valueChanged(int value);

    void on_sliderMediumRotation_valueChanged(int value);

    void on_sliderTopRotation_valueChanged(int value);

    void on_sliderGripper_valueChanged(int value);

    void on_spinBox_yPosition_valueChanged(int arg1);

    void on_spinBox_xPosition_valueChanged(int arg1);

    void on_spinBox_zPosition_valueChanged(int arg1);

    void on_actionOpen_animation_triggered();

    void on_spinBox_oneFrameCurrent_valueChanged(int arg1);

    void on_actionNew_animation_triggered();

    void on_pushButton_stop_clicked();

    void on_pushButton_resume_clicked();

    void on_verticalScrollBar_animationTimeline_valueChanged(int value);

    void on_pushButton_new_clicked();

    void on_actionSettings_triggered();

    void on_openGLWidget_resized();

    void on_actionFor_robot_triggered();

    void on_actionRaw_version_triggered();

    void on_actionDelete_triggered();

private:

    std::map<QString,int> defaultSettings;

    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
