#include <GL/glu.h>
#include <GL/gl.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stdio.h>

double degrees(double radians)
{
   return (radians * 180.0) / ((double) M_PI);
}

double radians(double degrees){
    return ((degrees*M_PI)/180 );
}




float baseAngle(float xTarget, float zTarget)
{

    float resAngle;

    float hip = sqrt( xTarget *xTarget+zTarget*zTarget);

    if (xTarget > 0) {
        if (zTarget < 0) {
            resAngle = 360 - (degrees(asin(-zTarget / hip)));
        } else if (zTarget > 0) {
            resAngle = degrees(asin(zTarget / hip));
        } else {
            resAngle = 0;
        }
    } else if (xTarget == 0) {
        if (zTarget > 0) {
            resAngle = 270;
        } else if (zTarget < 0) {
            resAngle = 90;
        } else {
            resAngle = 0;
        }
    } else {
        if (zTarget < 0) {
            resAngle = 180 + (degrees(asin(-zTarget / hip)));
        } else if (zTarget > 0) {
            resAngle = 180 - (degrees(asin(zTarget / hip)));
        } else {
            resAngle = 180;
        }
    }

    return resAngle;

}


void removeEqual(std::vector<std::vector<float>*> &inputVectorsPack){

    unsigned int columnIterator =0;



    while(columnIterator < inputVectorsPack[0]->size()){

        bool allElementsEqual = true;

        for(unsigned int rowIterator = 0;rowIterator<inputVectorsPack.size();rowIterator++){

          allElementsEqual = (allElementsEqual && (inputVectorsPack[rowIterator][0][columnIterator] == inputVectorsPack[rowIterator][0][columnIterator+1]));

        }
        if(allElementsEqual){

            for(unsigned int rowIterator = 0;rowIterator<inputVectorsPack.size();rowIterator++){


                std::vector<float>::iterator iterator = inputVectorsPack[rowIterator]->begin() + columnIterator;

                inputVectorsPack[rowIterator]->erase(iterator);

            }
        }else{

            columnIterator++;

        }
    }


}


void smoothVector(std::vector<float> &inputVector){

    int firstPointIndex = 0;
    int lastPointIndex;
    int lastBiggestNumber;
    int lastSmallestNumber;
    int targetHigh = 0;
    int targetLow = 0;
    bool isIncrementing = false;
    bool notFirstTime = false;

    for(unsigned int i = 0;i<inputVector.size() - 1;i++){

        int firstValue = inputVector[i];

        int secondValue = inputVector[i+1];

        if(firstValue <= secondValue){

            lastBiggestNumber = secondValue;

            lastPointIndex = i+1;

          if(notFirstTime){

              if(!isIncrementing){

                  std::fill(inputVector.begin()+firstPointIndex,inputVector.begin()+lastPointIndex,targetLow);

                  inputVector[lastPointIndex] = lastBiggestNumber;

                  targetHigh = lastBiggestNumber;

                  firstPointIndex = i;

              }else{

                  lastPointIndex  = i;

              }
          }

            notFirstTime = true;

            isIncrementing = true;

        }else if(firstValue > secondValue){

            lastSmallestNumber = secondValue;

            if(notFirstTime){

                if(isIncrementing){

                    std::fill(inputVector.begin()+firstPointIndex,inputVector.begin()+lastPointIndex,targetHigh);

                    inputVector[lastPointIndex] = lastSmallestNumber;

                    targetLow = lastSmallestNumber;

                    firstPointIndex = i;

                }else{

                    lastPointIndex  = i;
                }

            }

            notFirstTime = true;

            isIncrementing = false;
        }

    }

    if(isIncrementing){

        std::fill(inputVector.begin()+firstPointIndex,inputVector.begin()+lastPointIndex,targetHigh);

    }else{

        std::fill(inputVector.begin()+firstPointIndex,inputVector.begin()+lastPointIndex,targetLow);

    }

}



