#include "openGL/owidget.h"
#include <QtCore>
#include <QOpenGLWidget>
#include <GL/glu.h>
#include <GL/gl.h>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMouseEvent>
#include <QDebug>
#include <openGL/support.h>
#include <openGL/camera.h>
#include <QString>

owidget::owidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    this->update();
}

owidget::~owidget()
{

}

void owidget::initializeGL()
{
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-0.5,0.5,-0.5,0.5,1,1000000);
    glMatrixMode(GL_MODELVIEW);
}

void owidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glColor3f(1,1,1);
    glPushMatrix();

    //gluLookAt(mainCamera.xPosition + sceneMovementX,mainCamera.yPosition + sceneMovementY,mainCamera.zPosition + sceneMovementZ, sceneMovementX, sceneMovementY, sceneMovementZ, 0.0, 1.0, 0.0);

    gluLookAt(mainCamera.xPosition ,mainCamera.yPosition ,mainCamera.zPosition, 0,0,0, 0.0, 1.0, 0.0);

    scene();



    currentRobot->draw();


    glPopMatrix();

}

void owidget::resizeGL(int width, int height)
{
    glViewport(0,0,width,height);
}

void owidget::mouseDoubleClickEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {

        this->setDefaultView();
    }

}

void owidget::changeSize(int width,int height){
    this->resizeGL(width,height);
}


void owidget::setDefaultView(){
    mainCamera.zoomDistance = 500;
    mainCamera.verticalAngle = 45 ;
    mainCamera.horizontalAngle = 45;
    mainCamera.horizontalAngleIncrement = 0;
    mainCamera.verticalAngleIncrement = 0;
    mainCamera.updatePosition();
    sceneMovementX = 0;
    sceneMovementY = 0;
    sceneMovementZ = 0;
    owidget::update();
}

void owidget::mousePressEvent(QMouseEvent *event)
{

    if (event->button() == Qt::LeftButton) {
        moveVectorBeginningX = event->x();
        moveVectorBeginningY = event->y();
    }

    if (event->button() == Qt::RightButton) {
        dragVectorBeginningX = event->x();
        dragVectorBeginningY = event->y();
    }
}

void owidget::mouseMoveEvent(QMouseEvent *event)
{

     if(event->buttons() & Qt::LeftButton)
    {
        float xPart = event->x() - moveVectorBeginningX;
        float yPart = moveVectorBeginningY - event->y();

        sceneMovementY += yPart * moveSensetivity * cos(mainCamera.getElevationAngle());

        sceneMovementX += xPart * moveSensetivity * cos(mainCamera.getRotationAngle());
        sceneMovementZ += xPart * moveSensetivity * sin(mainCamera.getRotationAngle());
    }
    if (event->buttons() & Qt::RightButton)
    {
        mainCamera.horizontalAngle=-(dragVectorBeginningX-event->x())*dragSensitivity;
        mainCamera.verticalAngle=(dragVectorBeginningY-event->y())*dragSensitivity;
        mainCamera.updatePosition();
    }
    owidget::update();
}




void owidget::mouseReleaseEvent(QMouseEvent *event)
{


    if (event->button() & Qt::RightButton)
    {
        mainCamera.horizontalAngleIncrement+=mainCamera.horizontalAngle;
        mainCamera.verticalAngleIncrement+=mainCamera.verticalAngle;
        mainCamera.verticalAngle=0;
        mainCamera.horizontalAngle=0;
        mainCamera.updatePosition();

    }

    owidget::update();
}

void owidget::wheelEvent(QWheelEvent *event)
{

    mainCamera.zoomDistance+=(event->delta())*wheelSensitivity;
    mainCamera.updatePosition();
    owidget::update();

}

void owidget::scene()
{
    glBegin(GL_LINES);
            glColor3f(1,0,0);
            glVertex3f(-200,0,200);
            glVertex3f(200,0,200);

            glColor3f(0,1,0);
            glVertex3f(200,0,-200);
            glVertex3f(200,0,200);

            glColor3f(0,0,1);
            glVertex3f(-200,0,-200);
            glVertex3f(-200,0,200);

            glColor3f(1,0,1);
            glVertex3f(-200,0,-200);
            glVertex3f(200,0,-200);

    glEnd();
}

void owidget::coordVisual(float size)
{
    glColor3f(1,0,0);
    glBegin(GL_LINES);
        glVertex3f(0,0,0);
        glVertex3f(0,size,0);
    glEnd();
    glColor3f(0,1,0);
    glBegin(GL_LINES);
        glVertex3f(0,0,0);
        glVertex3f(size,0,0);
    glEnd();
    glColor3f(0,0,1);
    glBegin(GL_LINES);
        glVertex3f(0,0,0);
        glVertex3f(0,0,size);
    glEnd();
}


