#ifndef CAMERA_H
#define CAMERA_H


class camera
{
public:
    camera();

    float minimalZoom = 10;
    float zoomDistance = 500;
    float verticalAngle = 45 ;
    float horizontalAngle = 45;

    float horizontalAngleIncrement;
    float verticalAngleIncrement;

    float xPosition;
    float yPosition;
    float zPosition;
    float horizontalDistance;

    void updatePosition();
    void setAllZero();
    int counter;


    float getElevationAngle();
    float getRotationAngle();


protected:


    float returnIncluded(float inputNumber,float rangeLowBoundary,float rangeHighBoundary);


};

#endif // CAMERA_H
