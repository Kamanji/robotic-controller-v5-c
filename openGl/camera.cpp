#include "camera.h"
#include "owidget.h"
#include <QtCore>
#include <QOpenGLWidget>
#include <GL/glu.h>
#include <GL/gl.h>
#include <QDebug>
#include <openGL/support.h>

camera::camera()
{

}



float camera::getElevationAngle(){

    float elevationAngle = degrees(asin(this->yPosition/this->zoomDistance));

    return elevationAngle;

}

float camera::getRotationAngle(){

    return baseAngle(this->xPosition,this->zPosition);

}

float camera::returnIncluded(float inputNumber,float rangeLowBoundary,float rangeHighBoundary)
{
    if(inputNumber>rangeLowBoundary){
        if(inputNumber<rangeHighBoundary){
            return inputNumber;
        }
        else
        {
          return rangeHighBoundary;
        }
    }
    else
    {
        return rangeLowBoundary;
    }
}

void camera::setAllZero()
{

}

void camera::updatePosition()
{
    this->zoomDistance=(this->zoomDistance<=this->minimalZoom)?this->minimalZoom:this->zoomDistance;

    this->yPosition=this->zoomDistance*sin(radians(returnIncluded(this->verticalAngle+this->verticalAngleIncrement,-90,90)));

    this->horizontalDistance=this->zoomDistance*cos(radians(returnIncluded(this->verticalAngle+this->verticalAngleIncrement,-90,90)));

    this->zPosition=this->horizontalDistance*sin(radians(this->horizontalAngle+this->horizontalAngleIncrement));

    this->xPosition=this->horizontalDistance*cos(radians(this->horizontalAngle+this->horizontalAngleIncrement));

}
