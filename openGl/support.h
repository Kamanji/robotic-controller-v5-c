#ifndef SUPPORT_H
#define SUPPORT_H

#include <GL/glu.h>
#include <GL/gl.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <stdio.h>

double degrees(double radians);
double radians(double degrees);
float baseAngle(float xTarget, float zTarget);
void smoothVector(std::vector<float> &inputVector);
void removeEqual(std::vector<std::vector<float>*> &inputVectorsPack);


#endif // SUPPORT_H
