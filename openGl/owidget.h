#ifndef OWIDGET_H
#define OWIDGET_H

#include <QMainWindow>
#include <QWidget>
#include <QWheelEvent>
#include <QString>
#include <cstdlib>
#include <QOpenGLWidget>
#include <gl/GLU.h>
#include <gl/GL.h>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <openGL/camera.h>

#include <robots/dof6.h>
#include <robots/scara.h>
#include <robots/dof4.h>
#include <robots/gripper.h>

class owidget:public QOpenGLWidget



{
    Q_OBJECT
public:

    void coordVisual(float size);

    owidget(QWidget *parent = 0);
    ~owidget();

    void changeSize(int width,int height);

    void setXsceneMovement(int value);
    void setYsceneMovement(int value);
    void setZsceneMovement(int value);

    camera mainCamera;

    dof4 robot4dof;

    dof6 robot6dof;

    scara robotScara;

    robot* currentRobot = &robot4dof;

    void setDefaultView();

protected:

    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

private:

    float dragSensitivity = 0.75;
    float moveSensetivity = 0.03;
    float wheelSensitivity = 0.100;
    float dragVectorBeginningX;
    float dragVectorBeginningY;
    float moveVectorBeginningX;
    float moveVectorBeginningY;
    float sceneMovementX;
    float sceneMovementY;
    float sceneMovementZ;

    void scene();


};

#endif // OWIDGET_H
