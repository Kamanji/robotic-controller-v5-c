#ifndef ROBOT_H
#define ROBOT_H

#include <robots/gripper.h>
#include <QString>

class robot
{
public:
    robot();

    std::string id;

    void setMaxHeight(int maxHeightValue);

    float getLen1();
    float getLen2();
    float getLen3();
    float getLen4();
    float getJointRad();

    void setLen1(float len);
    void setLen2(float len);
    void setLen3(float len);
    void setLen4(float len);
    void setJointRad(float rad);

    float getAngleBaseBend();
    float getAngleMediumBend();
    float getAngleTopBend();
    float getAngleBaseRotation();
    float getAngleMediumRotation();
    float getAngleTopRotation();
    float getAngleGripper();

    void setAngleBaseBend(float angle);
    void setAngleMediumBend(float angle);
    void setAngleTopBend(float angle);
    void setAngleBaseRotation(float angle);
    void setAngleMediumRotation(float angle);
    void setAngleTopRotation(float angle);
    void setAngleGripper(float angle);

    void setEndEffectorX(float coordinate);
    void setEndEffectorY(float coordinate);
    void setEndEffectorZ(float coordinate);

    float getEndEffectorX();
    float getEndEffectorY();
    float getEndEffectorZ();

    virtual void draw() = 0;
    virtual void updateCoordinates() = 0;
    virtual void updateAngles() = 0;


    float baseCoords(float len, float angle,char mod);


protected:

    int maxHeight;

    float coordinatesArray[12];

    float lenght[4];
    float jointRad;

    float angleBaseBend;
    float angleMediumBend;
    float angleTopBend;
    float angleBaseRotation;
    float angleMediumRotation;
    float angleTopRotation;
    float angleGripper;


};

#endif // ROBOT_H
