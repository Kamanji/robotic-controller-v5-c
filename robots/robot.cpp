#include "robot.h"

robot::robot()
{

}


float robot::baseCoords(float len, float angle,char mod)
{
    float x,z;

    if(angle<=90){
        x = len*cos(radians(angle));
        z = len*sin(radians(angle));
    }else if(angle>90&&angle<=180){
        x = -len*cos(radians(180-angle));
        z = len*sin(radians(180-angle));
    }else if(angle>180&&angle<=270){
        x = -len*cos(radians(angle-180));
        z = -len*sin(radians(angle-180));
    }else{
        x = len*cos(radians(360-angle));
        z = -len*sin(radians(360-angle));
    }
    if(mod=='x'){
        return x;
    }else{
        return z;
    }
}
void robot::setMaxHeight(int maxHeightValue){
    this->maxHeight = maxHeightValue;
}

float robot::getAngleGripper(){
    return(this->angleGripper);
}
float robot::getAngleBaseBend(){
    return(this->angleBaseBend);
}

float robot::getAngleMediumBend(){
  return(this->angleMediumBend);
}

float robot::getAngleTopBend(){
  return(this->angleTopBend);
}

float robot::getAngleBaseRotation(){
  return(-this->angleBaseRotation);
}

float robot::getAngleMediumRotation(){
  return(this->angleMediumRotation);
}

float robot::getAngleTopRotation(){
  return(this->angleTopRotation);
}

void robot::setAngleBaseBend(float angle){
  this->angleBaseBend = angle;
}

void robot::setAngleMediumBend(float angle){
  this->angleMediumBend = angle;
}

void robot::setAngleTopBend(float angle){
  this->angleTopBend = angle;
}

void robot::setAngleBaseRotation(float angle){
  this->angleBaseRotation = -angle;
}

void robot::setAngleMediumRotation(float angle){
  this->angleMediumRotation = angle;
}

void robot::setAngleTopRotation(float angle){
  this->angleTopRotation = angle;
}
void robot::setAngleGripper(float angle){
  this->angleGripper  = angle;
}


void robot::setEndEffectorX(float coordinate){
  this->coordinatesArray[9] = coordinate;
}

void robot::setEndEffectorY(float coordinate){
  this->coordinatesArray[10] = coordinate;
}

void robot::setEndEffectorZ(float coordinate){
  this->coordinatesArray[11] = -coordinate;
}

float robot::getEndEffectorX(){
    return(this->coordinatesArray[9]);
}
float robot::getEndEffectorY(){
    return(this->coordinatesArray[10]);
}
float robot::getEndEffectorZ(){
    return(-this->coordinatesArray[11]);
}


float robot::getLen1(){
    return(this->lenght[0]);
}

float robot::getLen2(){
    return(this->lenght[1]);
}

float robot::getLen3(){
    return(this->lenght[2]);
}

float robot::getLen4(){
    return(this->lenght[3]);
}


void robot::setLen1(float len){
    this->lenght[0] = len;
}

void robot::setLen2(float len){
    this->lenght[1] = len;
}

void robot::setLen3(float len){
    this->lenght[2] = len;
}

void robot::setLen4(float len){
    this->lenght[3] = len;
}

void robot::setJointRad(float rad){
    this->jointRad = rad;
}
float robot::getJointRad(){
    return(this->jointRad);
}

