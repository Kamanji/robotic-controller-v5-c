#ifndef JOINT_H
#define JOINT_H

#include <openGL/support.h>

class joint
{
public:
    joint();

    virtual void draw();

    void setBaseCoordinates(float coordinateX, float coordinateY,float coordinateZ);

    void setBaseRotationAngles(float angleX,float angleY,float angleZ);

    void setColor(int red , int green , int blue);

    void setColorBorder(int redToSet,int greenToSet,int blueToSet);

    void setLength(float lengthValue);
    void setRadius(float radiusValue);
    void drawBox(float boxHeight,float boxDepth,float boxWidth,float borderRedColor,float borderGreenColor,float borderBlueColor,float mainRedColor,float mainGreenColor,float mainBlueColor);

protected:
    float baseCoonrinateX;
    float baseCoonrinateY;
    float baseCoonrinateZ;

    float baseRotationY;
    float baseRotationX;
    float baseRotationZ;

    float colorRed = 0.5;
    float colorGreen = 0.5;
    float colorBlue = 0.5;

    float colorBorderRed;
    float colorBorderGreen = 1;
    float colorBorderBlue;

    int length;
    int radius;

private:




};

#endif // JOINT_H
