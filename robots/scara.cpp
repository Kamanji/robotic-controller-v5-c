#include "scara.h"
#include <openGL/support.h>
#include <QDebug>


scara::scara()
{  

}


void scara::updateCoordinates()
{

    float len1 = this->getLen1();
    float len2 = this->getLen2();

    float mBend = this->getAngleMediumBend();
    float bBend = this->getAngleBaseBend();


    float mBend2 = ((mBend + 90)>180)?(270-mBend):mBend + 90;
    float AC = sqrt(len1*len1 + len2*len2 -2*len1*len2*cos(radians(mBend2)));
    float BAX = degrees(acos((len1*len1 + AC*AC - len2*len2)/(2*len1*AC)));

    float CAX = ((mBend + 90)>180)?bBend+BAX:bBend-BAX;

    this->coordinatesArray[3] = (len1 * cos(radians(-bBend)));
    this->coordinatesArray[5] = (len1 * sin(radians(-bBend)));

    this->setEndEffectorX(AC * cos(radians(CAX)));
    this->setEndEffectorZ(-AC * sin(radians(CAX)));

}

void scara::updateAngles()
{

    float len1 = this->getLen1();
    float len2 = this->getLen2();

    int x = this->getEndEffectorX();
    int z = this->getEndEffectorZ();

    float AC = sqrt(x*x+z*z);

    float ABC = degrees(acos((len1*len1 + len2*len2 - AC*AC)/(2*len1*len2)));
    float BAC = degrees(acos((len1*len1 + AC*AC - len2*len2)/(2*len1*AC)));

    this->setAngleMediumBend(ABC+BAC);

    this->setAngleBaseBend(baseAngle(x,z));

}

void scara::draw()
{

    this->updateCoordinates();

    axisJoint->setLength(this->maxHeight);
    axisJoint->setRadius(this->getJointRad());
    axisJoint->setBaseRotationAngles(0,0,90);
    axisJoint->draw();

    baseJoint->setBaseCoordinates(0,this->getAngleBaseRotation(),0);
    baseJoint->setBaseRotationAngles(0,this->getAngleBaseBend(),0);
    baseJoint->setRadius(this->getJointRad());
    baseJoint->setLength(this->getLen1());
    baseJoint->draw();

    mediumJoint->setBaseCoordinates(this->coordinatesArray[3],this->getAngleBaseRotation(),this->coordinatesArray[5]);
    mediumJoint->setRadius(this->getJointRad());
    mediumJoint->setLength(this->getLen2());
    mediumJoint->setBaseRotationAngles(0, -90 + angleBaseBend + angleMediumBend,0);
    mediumJoint->draw();

    mainGripper->setLength(this->getLen3());
    mainGripper->setDistance(this->getAngleGripper()/5);
    mainGripper->setBaseRotationAngles(0, -90 + angleBaseBend + angleMediumBend,-90);
    mainGripper->setBaseCoordinates(this->getEndEffectorX(),this->getAngleBaseRotation(),this->getEndEffectorZ());
    mainGripper->setRadius(this->getJointRad());
    mainGripper->draw();

}
