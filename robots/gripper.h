#ifndef GRIPPER_H
#define GRIPPER_H

#include <robots/joint.h>

class gripper:public joint
{
public:
    void setDistance(float distanceToSet);
    void draw();
    gripper();
private:
    float distance;


};

#endif // GRIPPER_H
