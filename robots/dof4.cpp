#include "dof4.h"
#include <openGL/support.h>
#include <QDebug>
#include <math.h>
dof4::dof4()
{
    this->setLen1(100);
    this->setLen2(100);
    this->setAngleBaseBend(45);
    this->setAngleMediumBend(45);
    this->jointRad = 1;
    this->id = "dof4";
    this->updateCoordinates();

}


void dof4::updateCoordinates()
{

    float angleBaseRotation = this->angleBaseRotation;
    float angleBaseBend = this->angleBaseBend;
    float angleMediumBend = this->angleMediumBend;

    float AB = this->getLen1();
    float BC = this->getLen2();

    float yb = sin(radians(angleBaseBend)) * AB;
    float AD = sqrt(AB * AB - yb * yb);
    float zb = sin(radians(angleBaseRotation)) * AD;
    float xb = cos(radians(angleBaseRotation)) * AD;

    zb=(angleBaseBend>90)?-zb:zb;
    xb=(angleBaseBend>90)?-xb:xb;

    float AC = sqrt(AB * AB + BC * BC - 2 * AB * BC * cos(radians(180 - angleBaseBend + angleMediumBend)));
    float BAC = degrees(acos((AB * AB + AC * AC - BC * BC) / (2 * AB * AC)));
    float CAF = ((180 - angleBaseBend + angleMediumBend) > 180) ? angleBaseBend + BAC : angleBaseBend - BAC;

    float yc = sin(radians(CAF)) * AC;
    float AF = sqrt(AC * AC - yc * yc);

    float zc = sin(radians(angleBaseRotation)) * AF;
    float xc = cos(radians(angleBaseRotation)) * AF;

    xc = (CAF > 90) ? -1 * xc : xc;
    zc = (CAF > 90) ? -1 * zc : zc;

    this->coordinatesArray[3] = xb ;
    this->coordinatesArray[4] = yb ;
    this->coordinatesArray[5] = zb ;

    this->setEndEffectorX(xc);
    this->setEndEffectorY(yc);
    this->setEndEffectorZ(zc);



}

void dof4::updateAngles()
{

    float xTarget = this->getEndEffectorX();
    float yTarget = this->getEndEffectorY();
    float zTarget = this->getEndEffectorZ();

    float AB = this->getLen1();
    float BC = this->getLen2();

    float AG = sqrt(xTarget * xTarget + zTarget * zTarget);
    float AC = sqrt(AG * AG + yTarget * yTarget);
    float ABC = degrees(acos((AB * AB + BC * BC - AC * AC)/(2 * AB * BC)));
    float BAC = degrees(acos((AB * AB + AC * AC - BC * BC)/(2 * AB * AC)));
    float CAG = degrees(asin(yTarget / AC));
    float BAG = BAC + CAG ;

    this->angleBaseBend = (std::isnan(BAG))? this->angleBaseBend:BAG;
    this->angleMediumBend = (std::isnan(-180 + ABC + BAG))? this->angleMediumBend  :  -180 + ABC + BAG;
    this->angleBaseRotation = (std::isnan(baseAngle( xTarget ,zTarget)))?  this->angleBaseRotation  : baseAngle( xTarget ,zTarget);

}

void dof4::draw()
{
    this->updateCoordinates();

    this->baseJoint->setRadius(this->jointRad);
    this->middleJoint->setRadius(this->jointRad);
    this->mainGripper->setRadius(this->jointRad);

    this->baseJoint->setLength(this->getLen1());
    this->middleJoint->setLength(this->getLen2());
    this->mainGripper->setLength(this->getLen3());

    this->baseJoint->setBaseCoordinates(0,0,0);
    this->baseJoint->setBaseRotationAngles(0,this->angleBaseRotation,this->angleBaseBend);
    this->baseJoint->draw();

    this->middleJoint->setBaseCoordinates(this->coordinatesArray[3],this->coordinatesArray[4],-1 * this->coordinatesArray[5]);
    this->middleJoint->setBaseRotationAngles(0,this->angleBaseRotation,this->angleMediumBend);
    this->middleJoint->draw();

    this->mainGripper->setBaseCoordinates(this->getEndEffectorX(),this->getEndEffectorY(),-1 * this->getEndEffectorZ());
    this->mainGripper->setBaseRotationAngles(0,this->angleBaseRotation,-90);
    this->mainGripper->setDistance(this->angleGripper/5);
    this->mainGripper->draw();

}

