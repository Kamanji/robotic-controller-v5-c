#ifndef DOF6_H
#define DOF6_H

#include <QObject>
#include <robots/robot.h>

class dof6: public robot
{


public:

    dof6();

    joint* baseJoint = new joint();
    joint* middleJoint = new joint();
    joint* topJoint = new joint();
    gripper* mainGripper = new gripper();


    void updateCoordinates();
    void updateAngles();
    void draw();
    void drawJointWithGripper();

};

#endif // DOF6_H
