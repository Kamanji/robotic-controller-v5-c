#include "dof6.h"
#include <openGL/support.h>
#include <QDebug>


dof6::dof6()
{

    this->setLen1(100);
    this->setLen2(100);
    this->setLen3(100);
    this->setLen4(20);
    this->angleBaseBend =45;
    this->angleBaseRotation = 45;
    this->angleMediumBend = 45;
    this->jointRad = 1;
    this->id = "dof6";
    this->updateCoordinates();


}

void dof6::updateCoordinates()
{

        float len1 =this->getLen1();
        float len2 =this->getLen2();
        float len3 =this->getLen3();


        // Calculating position of second joint start position
        float yb = sin(radians(angleBaseBend)) * len1;
        float AT = sqrt(len1 * len1 - yb * yb);
        float zb = sin(radians(angleBaseRotation)) * AT;
        float xb = cos(radians(angleBaseRotation)) * AT;

        zb=(angleBaseBend>90)?-zb:zb;
        xb=(angleBaseBend>90)?-xb:xb;

        coordinatesArray[3]=xb;
        coordinatesArray[4]=yb;
        coordinatesArray[5]=zb;

        //Calculating position of third joint start position

        float AC = sqrt(len1 * len1 + len2 * len2 - 2 * len1 * len2 * cos(radians(90 + angleMediumBend)));
        float BAC = degrees(acos((len1 * len1 + AC * AC - len2 * len2) / (2 * len1 * AC)));
        float CAF = ((90 + angleMediumBend) > 180) ? angleBaseBend + BAC : angleBaseBend - BAC;

        float yc = sin(radians(CAF)) * AC;
        float AF = sqrt(AC * AC - yc * yc);

        float zc = sin(radians(angleBaseRotation)) * AF;
        float xc = cos(radians(angleBaseRotation)) * AF;

        xc = (CAF > 90) ? -1 * xc : xc;
        zc = (CAF > 90) ? -1 * zc : zc;

        coordinatesArray[6]=xc;
        coordinatesArray[7]=yc;
        coordinatesArray[8]=zc;

        // Fouth joint

        float BAD, AD, DAG;

        if ((angleMediumBend + 90) <= 180) {

            if ((90 + angleTopBend) <= 180) {

                float BD = sqrt(len2 * len2 + len3 * len3 - 2 * len2 * len3 * cos(radians(90 + angleTopBend)));
                float CBD = degrees(acos((len2 * len2 + BD * BD - len3 * len3) / (2 * len2 * BD)));
                float ABD = (90 + angleMediumBend) - CBD;
                AD = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(ABD)));
                BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));
                DAG = angleBaseBend - BAD;

            } else {

                float BD = sqrt(len2 * len2 + len3 * len3 - 2 * len2 * len3 * cos(radians(270 - angleTopBend)));
                float DBC = degrees(acos((BD * BD + len2 * len2 - len3 * len3) / (2 * BD * len2)));
                float ABD = 90 + angleMediumBend + DBC;

                if (ABD >= 180) {
                    AD = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(360 - ABD)));
                    BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));
                    DAG = angleBaseBend + BAD;
                } else {
                    AD = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(ABD)));
                    BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));
                    DAG = angleBaseBend - BAD;
                }

            }
        } else {
            if ((90 + angleTopBend) <= 180) {

                float BD = sqrt(len2 * len2 + len3 * len3 - 2 * len2 * len3 * cos(radians(90 + angleTopBend)));
                float CBD = degrees(acos((len2 * len2 + BD * BD - len3 * len3) / (2 * len2 * BD)));
                float ABD = (90 + angleMediumBend) - CBD;

                if(ABD>=180){
                    AD = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(ABD)));
                    BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));
                    DAG = angleBaseBend + BAD;
                }else{
                    AD = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(ABD)));
                    BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));
                    DAG = angleBaseBend - BAD;
                }



            } else {

                float BD = sqrt(len2 * len2 + len3 * len3 - 2 * len2 * len3 * cos(radians(270 - angleTopBend)));
                float DBC = degrees(acos((BD * BD + len2 * len2 - len3 * len3) / (2 * BD * len2)));
                float ABD = 90 + angleMediumBend + DBC;
                AD = sqrt(len1 * len1 + BD * BD - 2 * len1 * BD * cos(radians(ABD)));
                BAD = degrees(acos((len1 * len1 + AD * AD - BD * BD) / (2 * len1 * AD)));
                DAG = angleBaseBend + BAD;

            }

        }

        float yd = sin(radians(DAG)) * AD;
        float AG = sqrt(AD * AD - yd * yd);

        float zd = sin(radians(angleBaseRotation)) * AG;
        float xd = cos(radians(angleBaseRotation)) * AG;

        xd = (DAG > 90) ? -1 * xd : xd;
        zd = (DAG > 90) ? -1 * zd : zd;

        this->setEndEffectorX(xd);
        this->setEndEffectorY(yd);
        this->setEndEffectorZ(zd);

}

void dof6::updateAngles()
{

    float xTarget = this->getEndEffectorX();
    float yTarget = this->getEndEffectorY();
    float zTarget = this->getEndEffectorZ();

    float AB = this->getLen1();
    float BC = this->getLen2();
    float CD = this->getLen3();

    float AG = sqrt( xTarget * xTarget + zTarget * zTarget );
    float AD = sqrt( AG * AG + yTarget * yTarget);
    float DAG = degrees(asin(yTarget/AD));

    float angleBaseRotation = baseAngle(xTarget,zTarget);

    for (int angleMediumBend = 90; angleMediumBend <= 180; angleMediumBend++) {
        for (int angleTopBend = 90; angleTopBend <= 180; angleTopBend++) {

            float BD = sqrt(BC * BC + CD * CD - 2 * BC * CD * cos(radians(angleTopBend)));
            float CBD = degrees(acos((BC * BC + BD * BD - CD * CD) / (2 * BC * BD)));
            float ADt = sqrt(AB * AB + BD * BD - 2 * AB * BD * cos(radians(angleMediumBend - CBD)));
            float BAD = degrees(acos((AB * AB + ADt * ADt - BD * BD) / (2 * AB * AD)));

            if ((AD + 0.2 > ADt) && (AD - 0.2 < ADt)) {

                    this->setAngleBaseBend((float)(DAG+BAD));
                    this->setAngleMediumBend((float)(angleMediumBend - 90));
                    this->setAngleTopBend((float)(angleTopBend - 90));
                    this->setAngleBaseRotation((float)(angleBaseRotation));

            }
        }
    }

}


void dof6::drawJointWithGripper(){
/*


    float angleBaseRotation=-this->getAngleBaseRotation();
    float angleBaseBend=this->getAngleBaseBend();
    float angleMediumBend=this->getAngleMediumBend();
    float angleTopBend=this->getAngleTopBend();
    float angleTopRotation=this->getAngleTopRotation();
    float anlgeMediumRotation=this->getAngleMediumRotation();
    int angleGripper=this->getAngleGripper();

    int topJointLenghth = this->getLen3();

    this->baseJoint->setRadius(this->jointRad);
    this->middleJoint->setRadius(this->jointRad);
    this->topJoint->setRadius(this->jointRad);
    this->mainGripper->setRadius(this->jointRad);

    this->baseJoint->setLength(this->getLen1());
    this->middleJoint->setLength(this->getLen2());
    this->topJoint->setLength(this->getLen3());
    this->mainGripper->setLength(this->getLen4());


    glPushMatrix();

    glTranslatef(coordinatesArray[6],coordinatesArray[7],-coordinatesArray[8]);
    glRotatef(0,1,0,0);
    glRotatef(angleBaseRotation,0,1,0);
    glRotatef(180 + angleBaseBend + angleMediumBend + angleTopBend,0,0,1);

    this->topJoint->setBaseCoordinates(0,0,0);
    this->topJoint->setBaseRotationAngles( 0,0,0);
    this->topJoint->draw();

    this->mainGripper->setBaseCoordinates(this->getLen3(),0,0);
    this->mainGripper->setBaseRotationAngles(angleTopRotation,angleMediumRotation,0);
    this->mainGripper->setDistance(angleGripper/5);
    this->mainGripper->draw();

    glPopMatrix();

*/
}


void dof6::draw()
{
    this->updateCoordinates();

    float angleBaseRotation=-this->getAngleBaseRotation();
    float angleBaseBend=this->getAngleBaseBend();
    float angleMediumBend=this->getAngleMediumBend();
    float angleTopBend=this->getAngleTopBend();
    float angleTopRotation=this->getAngleTopRotation();
    float anlgeMediumRotation=this->getAngleMediumRotation();
    int angleGripper=this->getAngleGripper();

    int topJointLenghth = this->getLen3();

    this->baseJoint->setRadius(this->jointRad);
    this->middleJoint->setRadius(this->jointRad);
    this->topJoint->setRadius(this->jointRad);
    this->mainGripper->setRadius(this->jointRad);

    this->baseJoint->setLength(this->getLen1());
    this->middleJoint->setLength(this->getLen2());
    this->topJoint->setLength(this->getLen3());
    this->mainGripper->setLength(this->getLen4());

    glPushMatrix();

        this->baseJoint->setBaseCoordinates(0,0,0);
        this->baseJoint->setBaseRotationAngles(0,angleBaseRotation,angleBaseBend);
        this->baseJoint->draw();

        this->middleJoint->setBaseCoordinates(coordinatesArray[3], coordinatesArray[4], -coordinatesArray[5]);
        this->middleJoint->setBaseRotationAngles(0 ,angleBaseRotation , -90 + angleBaseBend + angleMediumBend);
        this->middleJoint->draw();

            glPushMatrix();

            glTranslatef(coordinatesArray[6],coordinatesArray[7],-coordinatesArray[8]);
            glRotatef(0,1,0,0);
            glRotatef(angleBaseRotation,0,1,0);
            glRotatef(180 + angleBaseBend + angleMediumBend + angleTopBend,0,0,1);

            this->topJoint->setBaseCoordinates(0,0,0);
            this->topJoint->setBaseRotationAngles( 0,0,0);
            this->topJoint->draw();

            this->mainGripper->setBaseCoordinates(this->getLen3(),0,0);
            this->mainGripper->setBaseRotationAngles(angleTopRotation,angleMediumRotation-90,0);
            this->mainGripper->setDistance(angleGripper/5);
            this->mainGripper->draw();

            glPopMatrix();


    drawJointWithGripper();

    glPopMatrix();

}

