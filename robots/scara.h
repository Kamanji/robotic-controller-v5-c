#ifndef SCARA_H
#define SCARA_H

#include <QObject>
#include <robots/robot.h>

class scara: public robot
{

public:

    joint  * baseJoint  = new joint();
    joint  * mediumJoint  = new joint();
    joint  * axisJoint  = new joint();
    gripper  * mainGripper  = new gripper();

    scara();
    void updateCoordinates();
    void updateAngles();
    void draw();

};

#endif // SCARA_H
