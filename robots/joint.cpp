#include "joint.h"

joint::joint()
{
    this->length = 100;
    this->radius = 2;
}

    void joint::drawBox(float boxHeight, float boxDepth, float boxWidth, float borderRedColor, float borderGreenColor, float borderBlueColor, float mainRedColor, float mainGreenColor, float mainBlueColor){

        glColor3f(borderRedColor,borderGreenColor,borderBlueColor);
        glBegin(GL_LINE_LOOP);
            glVertex3f(0,-boxDepth,-boxWidth);
            glVertex3f(0,-boxDepth,boxWidth);
            glVertex3f(0,boxDepth,boxWidth);
            glVertex3f(0,boxDepth,-boxWidth);
        glEnd();
        glBegin(GL_LINES);
            glVertex3f(0,-boxDepth,-boxWidth);
            glVertex3f(boxHeight,-boxDepth,-boxWidth);
            glVertex3f(0,-boxDepth,boxWidth);
            glVertex3f(boxHeight,-boxDepth,boxWidth);
            glVertex3f(0,boxDepth,boxWidth);
            glVertex3f(boxHeight,boxDepth,boxWidth);
            glVertex3f(0,boxDepth,-boxWidth);
            glVertex3f(boxHeight,boxDepth,-boxWidth);
        glEnd();
        glBegin(GL_LINE_LOOP);
            glVertex3f(boxHeight,-boxDepth,-boxWidth);
            glVertex3f(boxHeight,-boxDepth,boxWidth);
            glVertex3f(boxHeight,boxDepth,boxWidth);
            glVertex3f(boxHeight,boxDepth,-boxWidth);
        glEnd();

        glBegin(GL_POLYGON);
            glColor3f(mainRedColor,mainGreenColor,mainBlueColor);
            glVertex3f(0,-boxDepth,-boxWidth);
            glVertex3f(0,-boxDepth,boxWidth);
            glVertex3f(0,boxDepth,boxWidth);
            glVertex3f(0,boxDepth,-boxWidth);
        glEnd();

        glBegin(GL_POLYGON);
            glColor3f(mainRedColor,mainGreenColor,mainBlueColor);
            glVertex3f(boxHeight,-boxDepth,-boxWidth);
            glVertex3f(boxHeight,-boxDepth,boxWidth);
            glVertex3f(boxHeight,boxDepth,boxWidth);
            glVertex3f(boxHeight,boxDepth,-boxWidth);
        glEnd();

        glBegin(GL_POLYGON);
            glColor3f(mainRedColor,mainGreenColor,mainBlueColor);
            glVertex3f(  0, -boxDepth, -boxWidth );
            glVertex3f(  0,  boxDepth, -boxWidth );
            glVertex3f( boxHeight,  boxDepth, -boxWidth );
            glVertex3f( boxHeight, -boxDepth, -boxWidth );
        glEnd();

        glBegin(GL_POLYGON);
            glColor3f(mainRedColor,mainGreenColor,mainBlueColor);
            glVertex3f( 0, -boxDepth, boxWidth );
            glVertex3f( 0,  boxDepth, boxWidth );
            glVertex3f( boxHeight,  boxDepth, boxWidth );
            glVertex3f( boxHeight, -boxDepth, boxWidth );
        glEnd();

        glBegin(GL_POLYGON);
            glColor3f(mainRedColor,mainGreenColor,mainBlueColor);
            glVertex3f( 0, -boxDepth, -boxWidth );
            glVertex3f( 0,  boxDepth, boxWidth );
            glVertex3f( boxHeight,  boxDepth,  boxWidth );
            glVertex3f( boxHeight, -boxDepth,  -boxWidth );
        glEnd();

        glBegin(GL_POLYGON);
            glColor3f(mainRedColor,mainGreenColor,mainBlueColor);
            glVertex3f( 0, -boxDepth,  -boxWidth );
            glVertex3f( 0,  boxDepth,  boxWidth );
            glVertex3f( boxHeight,  boxDepth, boxWidth );
            glVertex3f( boxHeight, -boxDepth, -boxWidth );
       glEnd();


    }

void joint::draw(){

    glPushMatrix();

    glTranslatef(this->baseCoonrinateX,this->baseCoonrinateY,this->baseCoonrinateZ);
    glRotatef(this->baseRotationX,1,0,0);
    glRotatef(this->baseRotationY,0,1,0);
    glRotatef(this->baseRotationZ,0,0,1);

    this->drawBox(this->length,this->radius,this->radius,this->colorBorderRed,this->colorBorderGreen,this->colorBorderBlue,this->colorRed,this->colorGreen,this->colorBlue);

    glPopMatrix();
}

void joint::setBaseCoordinates(float coordinateX, float coordinateY,float coordinateZ){
    this->baseCoonrinateX = coordinateX;
    this->baseCoonrinateY = coordinateY;
    this->baseCoonrinateZ = coordinateZ;
}

void joint::setBaseRotationAngles(float angleX,float angleY,float angleZ){
    this->baseRotationX = angleX;
    this->baseRotationY = angleY;
    this->baseRotationZ = angleZ;
}

void joint::setColor(int red , int green , int blue){
    this->colorBlue = blue/255;
    this->colorGreen = green/255;
    this->colorRed = red/255;
}
void joint::setLength(float lengthValue){
    this->length = lengthValue;
}

void joint::setRadius(float radiusValue){
    this->radius = radiusValue;
}

void joint::setColorBorder(int redToSet,int greenToSet,int blueToSet){
    this->colorBorderRed  = redToSet/255;
    this->colorBorderGreen = greenToSet/255;
    this->colorBorderBlue = blueToSet/255;
}
