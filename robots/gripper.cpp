#include "gripper.h"

gripper::gripper()
{
    this->length = 20;
    this->radius = 2;
}
 void gripper::draw(){


     glPushMatrix();

     glTranslatef(this->baseCoonrinateX,this->baseCoonrinateY,this->baseCoonrinateZ);
     glRotatef(this->baseRotationX,1,0,0);
     glRotatef(this->baseRotationY,0,1,0);
     glRotatef(this->baseRotationZ,0,0,1);


     drawBox(this->radius,this->radius,this->distance/2,0,1,0,0.5,0.5,0.5);

     glTranslatef(0,0,distance/2);

     this->drawBox(this->length,this->radius,this->radius,this->colorBorderRed,this->colorBorderGreen,this->colorBorderBlue,this->colorRed,this->colorGreen,this->colorBlue);

     glTranslatef(0,0,-distance);

     this->drawBox(this->length,this->radius,this->radius,this->colorBorderRed,this->colorBorderGreen,this->colorBorderBlue,this->colorRed,this->colorGreen,this->colorBlue);

     glPopMatrix();
 }

 void gripper::setDistance(float distanceToSet){
     this->distance = distanceToSet;
 }
