#ifndef DOF4_H
#define DOF4_H

#include <openGL/support.h>
#include <robots/robot.h>


class dof4: public robot
{

public:

    dof4();

    joint* baseJoint = new joint();
    joint* middleJoint = new joint();
    gripper* mainGripper = new gripper();

    void updateCoordinates();
    void updateAngles();
    void draw();

};

#endif // DOF4_H
