#-------------------------------------------------
#
# Project created by QtCreator 2016-10-02T10:46:26
#
#-------------------------------------------------

QT       += core gui opengl serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Robotics_controller
TEMPLATE = app


SOURCES += main.cpp\
    robots/dof6.cpp \
    openGl/support.cpp \
    robots/dof4.cpp \
    robots/scara.cpp \
    openGl/owidget.cpp \
    openGl/camera.cpp \
    robots/robot.cpp \
    robots/joint.cpp \
    robots/gripper.cpp \
    mainwindow/mainwindow.cpp \
    mainwindow/mainWin-robot/mainWin-OpenGL.cpp \
    mainwindow/settingsWindow/settingsdialog.cpp \
    mainwindow/mainWin-robot/mainWin-robotevent.cpp \
    mainwindow/mainWin-settings/mainWin-settings.cpp \
    mainwindow/mainWin-settings/mainWin-settingsslots.cpp \
    mainwindow/mainWin-animation/mainWin-animationevent.cpp \
    mainwindow/mainWin-animation/mainWin-animation.cpp \
    mainwindow/mainWin-settings/mainwin-loadsettings.cpp


HEADERS  += mainwindow.h \
    robots/dof6.h \
    openGl/support.h \
    robots/dof4.h \
    robots/scara.h \
    openGl/owidget.h \
    openGl/camera.h \
    robots/robot.h \
    robots/joint.h \
    robots/gripper.h \
    mainwindow/settingsWindow/settingsdialog.h

FORMS    += mainwindow.ui \
    mainwindow/settingsWindow/settingsdialog.ui

LIBS += -lOpengl32\
        -lglut -lGlu32

DISTFILES += \
    temp_code \
    tempCode \
    settings
