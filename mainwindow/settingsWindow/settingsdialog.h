#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>


namespace Ui {
class settingsDialog;
}

class settingsDialog : public QDialog
{
    Q_OBJECT

public:

    int maxCircleRotation = 3600;

    explicit settingsDialog(QWidget *parent = 0);
    ~settingsDialog();

public slots:

    void setValuesOnStartup(std::map<QString,int> valuesMap);

private slots:

    void on_pushButton_cancel_clicked();

    void on_pushButton_apply_clicked();

    void on_pushButton_save_clicked();

signals:

    void signalPassMap(std::map<QString,int> valuesMap);

private:
    Ui::settingsDialog *ui;
};

#endif // SETTINGSDIALOG_H
