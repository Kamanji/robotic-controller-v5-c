#include "settingsdialog.h"
#include "ui_settingsdialog.h"

settingsDialog::settingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::settingsDialog)
{
    ui->setupUi(this);
}

settingsDialog::~settingsDialog()
{
    delete ui;
}

void settingsDialog::on_pushButton_cancel_clicked()
{
    this->close();
}

void settingsDialog::on_pushButton_apply_clicked()
{
   std::map<QString,int> resultMap;

   resultMap["slider4dofBaseBendLow"]                    = ui->spinBox_baseBend_4dof_low->value();
   resultMap["slider4dofBaseBendHigh"]                   = ui->spinBox_baseBend_4dof_high->value();
   resultMap["slider4dofBaseRotationLow"]                = ui->spinBox_baseRotaion_4dof_low->value();
   resultMap["slider4dofBaseRotationHigh"]               = ui->spinBox_baseRotation_4dof_high->value();
   resultMap["slider4dofMediumBendLow"]                  = ui->spinBox_mediumBend_4dof_low->value();
   resultMap["slider4dofMediumBendHigh"]                 = ui->spinBox_mediumBend_4dof_high->value();
   resultMap["slider4dofGripperLow"]                     = ui->spinBox_gripper_4dof_low->value();
   resultMap["slider4dofGripperHigh"]                    = ui->spinBox_gripper_4dof_high->value();

   resultMap["slider6dofBaseBendLow"]                    = ui->spinBox_baseBend_6dof_low->value();
   resultMap["slider6dofBaseBendHigh"]                   = ui->spinBox_baseBend_6dof_high->value();
   resultMap["slider6dofBaseRotationLow"]                = ui->spinBox_baseRotaion_6dof_low->value();
   resultMap["slider6dofBaseRotationHigh"]               = ui->spinBox_baseRotation_6dof_high->value();
   resultMap["slider6dofMediumBendLow"]                  = ui->spinBox_mediumBend_6dof_low->value();
   resultMap["slider6dofMediumBendHigh"]                 = ui->spinBox_mediumBend_6dof_high->value();
   resultMap["slider6dofTopBendLow"]                     = ui->spinBox_topBend_6dof_low->value();
   resultMap["slider6dofTopBendHigh"]                    = ui->spinBox_topBend_6dof_high->value();
   resultMap["slider6dofMediumRotationLow"]              = ui->spinBox_mediumRotation_6dof_low->value();
   resultMap["slider6dofMediumRotationHigh"]             = ui->spinBox_mediumRotation_6dof_high->value();
   resultMap["slider6dofTopRotationLow"]                 = ui->spinBox_topRotation_6dof_low->value();
   resultMap["slider6dofTopRotationHigh"]                = ui->spinBox_mediumRotation_6dof_high->value();
   resultMap["slider6dofGripperLow"]                     = ui->spinBox_gripper_6dof_low->value();
   resultMap["slider6dofGripperHigh"]                    = ui->spinBox_gripper_6dof_high->value();

   resultMap["sliderScaraBaseBendLow"]                   = ui->spinBox_baseBend_scara_low->value();
   resultMap["sliderScaraBaseBendHigh"]                  = ui->spinBox_baseBend_scara_high->value();
   resultMap["sliderScaraMediumBendLow"]                 = ui->spinBox_mediumBend_scara_low->value();
   resultMap["sliderScaraMediumBendHigh"]                = ui->spinBox_mediumBend_scara_high->value();
   resultMap["sliderScaraGripperLow"]                    = ui->spinBox_gripper_scara_low->value();
   resultMap["sliderScaraGripperHigh"]                   = ui->spinBox_gripper_scara_high->value();

   resultMap["dof4firstJoint"]                           = ui->spinBox_4dof_firstJoint->value();
   resultMap["dof4secondJoint"]                          = ui->spinBox_4dof_secondJoint->value();

   resultMap["dof6firstJoint"]                           = ui->spinBox_6dof_firstJoint->value();
   resultMap["dof6secondJoint"]                          = ui->spinBox_6dof_secondJoint->value();
   resultMap["dof6thirdJoint"]                           = ui->spinBox_6dof_thirdJoint->value();
   resultMap["dof6gripper"]                              = ui->spinBox_6dof_gripper->value();

   resultMap["scaraFirstJoint"]                          = ui->spinBox_scara_firstJoint->value();
   resultMap["scaraSecondJoint"]                         = ui->spinBox_scara_secondJoint->value();
   resultMap["scaraMaxHeight"]                           = ui->spinBox_scara_maxHeight->value();
   resultMap["scaraGripperLength"]                        = ui->spinBox4dof_gripperLength->value();

   resultMap["scaraRadius"]                              = ui->spinBox_scara_radius->value();
   resultMap["dof4radius"]                               = ui->spinBox_4dof_radius->value();
   resultMap["dof6radius"]                               = ui->spinBox_6dof_radius->value();
   resultMap["dof4gripperLength"]                        = ui->spinBox4dof_gripperLength->value();


   emit signalPassMap(resultMap);
}



void settingsDialog::on_pushButton_save_clicked()
{
    on_pushButton_apply_clicked();
    on_pushButton_cancel_clicked();
}

void settingsDialog::setValuesOnStartup(std::map<QString,int> valuesMap){

    ui->spinBox_baseBend_4dof_low->setValue(valuesMap[              "slider4dofBaseBendLow"]);
    ui->spinBox_baseBend_4dof_high->setValue(valuesMap[             "slider4dofBaseBendHigh"]);
    ui->spinBox_baseRotaion_4dof_low->setValue(valuesMap[           "slider4dofBaseRotationLow"]);
    ui->spinBox_baseRotation_4dof_high->setValue(valuesMap[         "slider4dofBaseRotationHigh"]);
    ui->spinBox_mediumBend_4dof_low->setValue(valuesMap[            "slider4dofMediumBendLow"]);
    ui->spinBox_mediumBend_4dof_high->setValue(valuesMap[           "slider4dofMediumBendHigh"]);
    ui->spinBox_gripper_4dof_low->setValue(valuesMap[               "slider4dofGripperLow"]);
    ui->spinBox_gripper_4dof_high->setValue(valuesMap[              "slider4dofGripperHigh"]);

    ui->spinBox_baseBend_6dof_low->setValue(valuesMap[              "slider6dofBaseBendLow"]);
    ui->spinBox_baseBend_6dof_high->setValue(valuesMap[             "slider6dofBaseBendHigh"]);
    ui->spinBox_baseRotaion_6dof_low->setValue(valuesMap[           "slider6dofBaseRotationLow"]);
    ui->spinBox_baseRotation_6dof_high->setValue(valuesMap[         "slider6dofBaseRotationHigh"]);
    ui->spinBox_mediumBend_6dof_low->setValue(valuesMap[            "slider6dofMediumBendLow"]);
    ui->spinBox_mediumBend_6dof_high->setValue(valuesMap[           "slider6dofMediumBendHigh"]);
    ui->spinBox_topBend_6dof_low->setValue(valuesMap[               "slider6dofTopBendLow"]);
    ui->spinBox_topBend_6dof_high->setValue(valuesMap[              "slider6dofTopBendHigh"]);
    ui->spinBox_mediumRotation_6dof_low->setValue(valuesMap[        "slider6dofMediumRotationLow"]);
    ui->spinBox_mediumRotation_6dof_high->setValue(valuesMap[       "slider6dofMediumRotationHigh"]);
    ui->spinBox_topRotation_6dof_low->setValue(valuesMap[           "slider6dofTopRotationLow"]);
    ui->spinBox_mediumRotation_6dof_high->setValue(valuesMap[       "slider6dofTopRotationHigh"]);
    ui->spinBox_gripper_6dof_low->setValue(valuesMap[               "slider6dofGripperLow"]);
    ui->spinBox_gripper_6dof_high->setValue(valuesMap[              "slider6dofGripperHigh"]);

    ui->spinBox_baseBend_scara_low->setValue(valuesMap[             "sliderScaraBaseBendLow"]);
    ui->spinBox_baseBend_scara_high->setValue(valuesMap[            "sliderScaraBaseBendHigh"]);
    ui->spinBox_mediumBend_scara_low->setValue(valuesMap[           "sliderScaraMediumBendLow"]);
    ui->spinBox_mediumBend_scara_high->setValue(valuesMap[          "sliderScaraMediumBendHigh"]);
    ui->spinBox_gripper_scara_low->setValue(valuesMap[              "sliderScaraGripperLow"]);
    ui->spinBox_gripper_scara_high->setValue(valuesMap[             "sliderScaraGripperHigh"]);

    ui->spinBox_4dof_firstJoint->setValue(valuesMap[                "dof4firstJoint"]);
    ui->spinBox_4dof_secondJoint->setValue(valuesMap[               "dof4secondJoint"]);

    ui->spinBox_6dof_firstJoint->setValue(valuesMap[                "dof6firstJoint"]);
    ui->spinBox_6dof_secondJoint->setValue(valuesMap[               "dof6secondJoint"]);
    ui->spinBox_6dof_thirdJoint->setValue(valuesMap[                "dof6thirdJoint"]);
    ui->spinBox_6dof_gripper->setValue(valuesMap[                   "dof6gripper"]);

    ui->spinBox_scara_firstJoint->setValue(valuesMap[               "scaraFirstJoint"]);
    ui->spinBox_scara_secondJoint->setValue(valuesMap[              "scaraSecondJoint"]);
    ui->spinBox_scara_maxHeight->setValue(valuesMap[                "scaraMaxHeight"]);
    ui->spinBox_scaraGripperLength->setValue(valuesMap[             "scaraGripperLength"]);


    ui->spinBox_scara_radius->setValue(valuesMap[                   "scaraRadius"]);
    ui->spinBox_4dof_radius->setValue(valuesMap[                    "dof4radius"]);
    ui->spinBox_6dof_radius->setValue(valuesMap[                    "dof6radius"]);
    ui->spinBox4dof_gripperLength->setValue(valuesMap[              "dof4gripperLength"]);

}
