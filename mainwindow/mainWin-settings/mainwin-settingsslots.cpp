#include <mainwindow.h>

void MainWindow::slotSettingsMap(std::map<QString,int> settingMap){

    defaultSettings = settingMap;

    applySettings();

    ui->openGLWidget->update();

    this->setCoordinatesSpinBox();

}
