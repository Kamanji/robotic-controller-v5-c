#include "mainwindow.h"
#include <QSettings>
#include <QApplication>

void MainWindow::loadSettings(){

    QSettings mainWindowSettings("K.inc","Robotics controller");

    mainWindowSettings.beginGroup("mainWinowDefault");

    defaultSettings["slider4dofBaseBendLow"]             = mainWindowSettings.value("slider4dofBaseBendLow").toInt();
    defaultSettings["slider4dofBaseBendHigh"]            = mainWindowSettings.value("slider4dofBaseBendHigh").toInt();
    defaultSettings["slider4dofBaseRotationLow"]         = mainWindowSettings.value("slider4dofBaseRotationLow").toInt();
    defaultSettings["slider4dofBaseRotationHigh"]        = mainWindowSettings.value("slider4dofBaseRotationHigh").toInt();
    defaultSettings["slider4dofMediumBendLow"]           = mainWindowSettings.value("slider4dofMediumBendLow").toInt();
    defaultSettings["slider4dofMediumBendHigh"]          = mainWindowSettings.value("slider4dofMediumBendHigh").toInt();
    defaultSettings["slider4dofGripperLow"]              = mainWindowSettings.value("slider4dofGripperLow").toInt();
    defaultSettings["slider4dofGripperHigh"]             = mainWindowSettings.value("slider4dofGripperHigh").toInt();

    defaultSettings["slider6dofBaseBendLow"]             = mainWindowSettings.value("slider6dofBaseBendLow").toInt();
    defaultSettings["slider6dofBaseBendHigh"]            = mainWindowSettings.value("slider6dofBaseBendHigh").toInt();
    defaultSettings["slider6dofBaseRotationLow"]         = mainWindowSettings.value("slider6dofBaseRotationLow").toInt();
    defaultSettings["slider6dofBaseRotationHigh"]        = mainWindowSettings.value("slider6dofBaseRotationHigh").toInt();
    defaultSettings["slider6dofMediumBendLow"]           = mainWindowSettings.value("slider6dofMediumBendLow").toInt();
    defaultSettings["slider6dofMediumBendHigh"]          = mainWindowSettings.value("slider6dofMediumBendHigh").toInt();
    defaultSettings["slider6dofTopBendLow"]              = mainWindowSettings.value("slider6dofTopBendLow").toInt();
    defaultSettings["slider6dofTopBendHigh"]             = mainWindowSettings.value("slider6dofTopBendHigh").toInt();
    defaultSettings["slider6dofMediumRotationLow"]       = mainWindowSettings.value("slider6dofMediumRotationLow").toInt();
    defaultSettings["slider6dofMediumRotationHigh"]      = mainWindowSettings.value("slider6dofMediumRotationHigh").toInt();
    defaultSettings["slider6dofTopRotationLow"]          = mainWindowSettings.value("slider6dofTopRotationLow").toInt();
    defaultSettings["slider6dofTopRotationHigh"]         = mainWindowSettings.value("slider6dofTopRotationHigh").toInt();
    defaultSettings["slider6dofGripperLow"]              = mainWindowSettings.value("slider6dofGripperLow").toInt();
    defaultSettings["slider6dofGripperHigh"]             = mainWindowSettings.value("slider6dofGripperHigh").toInt();

    defaultSettings["sliderScaraBaseBendLow"]            = mainWindowSettings.value("sliderScaraBaseBendLow").toInt();
    defaultSettings["sliderScaraBaseBendHigh"]           = mainWindowSettings.value("sliderScaraBaseBendHigh").toInt();
    defaultSettings["sliderScaraMediumBendLow"]          = mainWindowSettings.value("sliderScaraMediumBendLow").toInt();
    defaultSettings["sliderScaraMediumBendHigh"]         = mainWindowSettings.value("sliderScaraMediumBendHigh").toInt();
    defaultSettings["sliderScaraGripperLow"]             = mainWindowSettings.value("sliderScaraGripperLow").toInt();
    defaultSettings["sliderScaraGripperHigh"]            = mainWindowSettings.value("sliderScaraGripperHigh").toInt();

    defaultSettings["dof4firstJoint"]                    = mainWindowSettings.value("dof4firstJoint").toInt();
    defaultSettings["dof4secondJoint"]                   = mainWindowSettings.value("dof4secondJoint").toInt();

    defaultSettings["dof6firstJoint"]                    = mainWindowSettings.value("dof6firstJoint").toInt();
    defaultSettings["dof6secondJoint"]                   = mainWindowSettings.value("dof6secondJoint").toInt();
    defaultSettings["dof6thirdJoint"]                    = mainWindowSettings.value("dof6thirdJoint").toInt();
    defaultSettings["dof6gripper"]                       = mainWindowSettings.value("dof6gripper").toInt();

    defaultSettings["scaraFirstJoint"]                   = mainWindowSettings.value("scaraFirstJoint").toInt();
    defaultSettings["scaraSecondJoint"]                  = mainWindowSettings.value("scaraSecondJoint").toInt();
    defaultSettings["scaraMaxHeight"]                     = mainWindowSettings.value("scaraMaxHeight").toInt();
    defaultSettings["scaraGripperLength"]                = mainWindowSettings.value("scaraGripperLength").toInt();

    defaultSettings["dof4radius"]                        = mainWindowSettings.value("dof4radius").toInt();
    defaultSettings["scaraRadius"]                       = mainWindowSettings.value("scaraRadius").toInt();
    defaultSettings["dof6radius"]                        = mainWindowSettings.value("dof6radius").toInt();
    defaultSettings["dof4gripperLength"]                 = mainWindowSettings.value("dof4gripperLength").toInt();


    mainWindowSettings.endGroup();

}

void MainWindow::saveSettings(){

    QSettings mainWindowSettings("K.inc","Robotics controller");

    mainWindowSettings.beginGroup("mainWinowDefault");

    mainWindowSettings.setValue("slider4dofBaseBendLow",               defaultSettings["slider4dofBaseBendLow"]);
    mainWindowSettings.setValue("slider4dofBaseBendHigh",              defaultSettings["slider4dofBaseBendHigh"]);
    mainWindowSettings.setValue("slider4dofBaseRotationLow",           defaultSettings["slider4dofBaseRotationLow"]);
    mainWindowSettings.setValue("slider4dofBaseRotationHigh",          defaultSettings["slider4dofBaseRotationHigh"]);
    mainWindowSettings.setValue("slider4dofMediumBendLow",             defaultSettings["slider4dofMediumBendLow"]);
    mainWindowSettings.setValue("slider4dofMediumBendHigh",            defaultSettings["slider4dofMediumBendHigh"]);
    mainWindowSettings.setValue("slider4dofGripperLow",                defaultSettings["slider4dofGripperLow"]);
    mainWindowSettings.setValue("slider4dofGripperHigh",               defaultSettings["slider4dofGripperHigh"]);

    mainWindowSettings.setValue("slider6dofBaseBendLow",               defaultSettings["slider6dofBaseBendLow"]);
    mainWindowSettings.setValue("slider6dofBaseBendHigh",              defaultSettings["slider6dofBaseBendHigh"]);
    mainWindowSettings.setValue("slider6dofBaseRotationLow",           defaultSettings["slider6dofBaseRotationLow"]);
    mainWindowSettings.setValue("slider6dofBaseRotationHigh",          defaultSettings["slider6dofBaseRotationHigh"]);
    mainWindowSettings.setValue("slider6dofMediumBendLow",             defaultSettings["slider6dofMediumBendLow"]);
    mainWindowSettings.setValue("slider6dofMediumBendHigh",            defaultSettings["slider6dofMediumBendHigh"]);
    mainWindowSettings.setValue("slider6dofTopBendLow",                defaultSettings["slider6dofTopBendLow"]);
    mainWindowSettings.setValue("slider6dofTopBendHigh",               defaultSettings["slider6dofTopBendHigh"]);
    mainWindowSettings.setValue("slider6dofMediumRotationLow",         defaultSettings["slider6dofMediumRotationLow"]);
    mainWindowSettings.setValue("slider6dofMediumRotationHigh",        defaultSettings["slider6dofMediumRotationHigh"]);
    mainWindowSettings.setValue("slider6dofTopRotationLow",            defaultSettings["slider6dofTopRotationLow"]);
    mainWindowSettings.setValue("slider6dofTopRotationHigh",           defaultSettings["slider6dofTopRotationHigh"]);
    mainWindowSettings.setValue("slider6dofGripperLow",                defaultSettings["slider6dofGripperLow"]);
    mainWindowSettings.setValue("slider6dofGripperHigh",               defaultSettings["slider6dofGripperHigh"]);

    mainWindowSettings.setValue("sliderScaraBaseBendLow",              defaultSettings["sliderScaraBaseBendLow"]);
    mainWindowSettings.setValue("sliderScaraBaseBendHigh",             defaultSettings["sliderScaraBaseBendHigh"]);
    mainWindowSettings.setValue("sliderScaraMediumBendLow",            defaultSettings["sliderScaraMediumBendLow"]);
    mainWindowSettings.setValue("sliderScaraMediumBendHigh",           defaultSettings["sliderScaraMediumBendHigh"]);
    mainWindowSettings.setValue("sliderScaraGripperLow",               defaultSettings["sliderScaraGripperLow"]);
    mainWindowSettings.setValue("sliderScaraGripperHigh",              defaultSettings["sliderScaraGripperHigh"]);

    mainWindowSettings.setValue("dof4firstJoint",                      defaultSettings["dof4firstJoint"]);
    mainWindowSettings.setValue("dof4secondJoint",                     defaultSettings["dof4secondJoint"]);

    mainWindowSettings.setValue("dof6firstJoint",                      defaultSettings["dof6firstJoint"]);
    mainWindowSettings.setValue("dof6secondJoint",                     defaultSettings["dof6secondJoint"]);
    mainWindowSettings.setValue("dof6thirdJoint",                      defaultSettings["dof6thirdJoint"]);
    mainWindowSettings.setValue("dof6gripper",                         defaultSettings["dof6gripper"]);

    mainWindowSettings.setValue("scaraFirstJoint",                     defaultSettings["scaraFirstJoint"]);
    mainWindowSettings.setValue("scaraSecondJoint",                    defaultSettings["scaraSecondJoint"]);
    mainWindowSettings.setValue("scaraMaxHeight",                      defaultSettings["scaraMaxHeight"]);
    mainWindowSettings.setValue("scaraGripperLength",                  defaultSettings["scaraGripperLength"]);


    mainWindowSettings.setValue("dof4radius",                          defaultSettings["dof4radius"]);
    mainWindowSettings.setValue("scaraRadius",                         defaultSettings["scaraRadius"]);
    mainWindowSettings.setValue("dof6radius",                          defaultSettings["dof6radius"]);
    mainWindowSettings.setValue("dof4gripperLength",                   defaultSettings["dof4gripperLength"]);

    mainWindowSettings.endGroup();

}

