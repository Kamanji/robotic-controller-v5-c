#include "mainwindow.h"
#include <QDialog>
#include <mainwindow/settingsWindow/settingsdialog.h>
#include <QObject>

void MainWindow::on_actionSettings_triggered()
{

    connect(settingsdialog,SIGNAL(signalPassMap(std::map<QString,int>)),this,SLOT(slotSettingsMap(std::map<QString,int>)));

    connect(this,SIGNAL(setCurrentSettings(std::map<QString,int>)),settingsdialog,SLOT(setValuesOnStartup(std::map<QString,int>)));

    settingsdialog->show();

    emit setCurrentSettings(defaultSettings);

}

void MainWindow::applySettings(){

    if(currentMode == "4dof"){

        ui->openGLWidget->currentRobot->setLen1(defaultSettings[         "dof4firstJoint"]);
        ui->openGLWidget->currentRobot->setLen2(defaultSettings[         "dof4secondJoint"]);
        ui->openGLWidget->currentRobot->setJointRad(defaultSettings[     "dof4radius"]);
        ui->openGLWidget->currentRobot->setLen3(defaultSettings[         "dof4gripperLength"]);

        ui->sliderBaseBend->setRange(defaultSettings[                    "slider4dofBaseBendLow"],defaultSettings[        "slider4dofBaseBendHigh"]);
        ui->sliderBaseRotation->setRange(defaultSettings[                "slider4dofBaseRotationLow"],defaultSettings[    "slider4dofBaseRotationHigh"]);
        ui->sliderMediumBend->setRange(defaultSettings[                  "slider4dofMediumBendLow"],defaultSettings[      "slider4dofMediumBendHigh"]);
        ui->sliderGripper->setRange(defaultSettings[                     "slider4dofGripperLow"],defaultSettings[         "slider4dofGripperHigh"]);

    }else if(currentMode == "6dof"){

        ui->openGLWidget->currentRobot->setLen1(defaultSettings[         "dof6firstJoint"]);
        ui->openGLWidget->currentRobot->setLen2(defaultSettings[         "dof6secondJoint"]);
        ui->openGLWidget->currentRobot->setLen3(defaultSettings[         "dof6thirdJoint"]);
        ui->openGLWidget->currentRobot->setLen4(defaultSettings[         "dof6gripper"]);
        ui->openGLWidget->currentRobot->setJointRad(defaultSettings[     "dof6radius"]);

        ui->sliderBaseBend->setRange(defaultSettings[                    "slider6dofBaseBendLow"],defaultSettings[        "slider6dofBaseBendHigh"]);
        ui->sliderBaseRotation->setRange(defaultSettings[                "slider6dofBaseRotationLow"],defaultSettings[    "slider6dofBaseRotationHigh"]);
        ui->sliderMediumBend->setRange(defaultSettings[                  "slider6dofMediumBendLow"],defaultSettings[      "slider6dofMediumBendHigh"]);
        ui->sliderTopBend->setRange(defaultSettings[                     "slider6dofTopBendLow"],defaultSettings[         "slider6dofTopBendHigh"]);
        ui->sliderMediumRotation->setRange(defaultSettings[              "slider6dofMediumRotationLow"],defaultSettings[  "slider6dofMediumRotationHigh"]);
        ui->sliderTopRotation->setRange(defaultSettings[                 "slider6dofTopRotationLow"],defaultSettings[     "slider6dofTopRotationHigh"]);
        ui->sliderGripper->setRange(defaultSettings[                     "slider6dofGripperLow"],defaultSettings[         "slider6dofGripperHigh"]);


    }else if(currentMode == "scara"){

        ui->openGLWidget->currentRobot->setMaxHeight(defaultSettings[    "scaraMaxHeight"]);
        ui->openGLWidget->currentRobot->setLen1(defaultSettings[         "scaraFirstJoint"]);
        ui->openGLWidget->currentRobot->setLen2(defaultSettings[         "scaraSecondJoint"]);
        ui->openGLWidget->currentRobot->setLen3(defaultSettings[         "scaraGripperLength"]);
        ui->openGLWidget->currentRobot->setJointRad(defaultSettings[     "scaraRadius"]);


        ui->sliderBaseRotation->setRange(0, defaultSettings[             "scaraMaxHeight"]);
        ui->sliderBaseBend->setRange(defaultSettings[                    "sliderScaraBaseBendLow"],defaultSettings[       "sliderScaraBaseBendHigh"]);
        ui->sliderMediumBend->setRange(defaultSettings[                  "sliderScaraMediumBendLow"],defaultSettings[     "sliderScaraMediumBendHigh"]);
        ui->sliderGripper->setRange(defaultSettings[                     "sliderScaraGripperLow"],defaultSettings[        "sliderScaraGripperHigh"]);

        qDebug()<<defaultSettings["sliderScaraMediumBendLow"];


    }
}
