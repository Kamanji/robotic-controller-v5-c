#include "mainwindow.h"
#include <QShortcut>
#include <QKeySequence>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->on_action4dof_triggered();
    ui->openGLWidget->setDefaultView();

    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_R), this, SLOT(on_pushButton_resume_clicked()));

    this->loadSettings();
    this->applySettings();

}

MainWindow::~MainWindow()
{
    this->saveSettings();
    delete ui;
}

void MainWindow::on_openGLWidget_resized()
{
    ui->openGLWidget->changeSize(ui->openGLWidget->width(),ui->openGLWidget->height());
}
