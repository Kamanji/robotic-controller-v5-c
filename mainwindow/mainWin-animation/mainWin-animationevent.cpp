#include "mainwindow.h"
#include "openGl/support.h"



void MainWindow::on_actionDelete_triggered()
{
    animationIsActive = false;

    ui->spinBox_oneFrameCurrent->setValue(0);
    ui->spinBox_oneFrameCurrent->setDisabled(true);
    ui->verticalScrollBar_animationTimeline->setDisabled(true);


    clearAnimation();
}

void MainWindow::on_spinBox_oneFrameCurrent_valueChanged(int arg1){


    animationIsActive = false;

    ui->openGLWidget->currentRobot->setAngleBaseRotation(baseRotation[arg1]);

    ui->openGLWidget->currentRobot->setAngleBaseBend(baseBend[arg1]);
    ui->openGLWidget->currentRobot->setAngleMediumBend(mediumBend[arg1]);
    ui->openGLWidget->currentRobot->setAngleGripper(gripper[arg1]);
    ui->openGLWidget->currentRobot->setAngleMediumRotation(mediumRotation[arg1]);
    ui->openGLWidget->currentRobot->setAngleTopBend(topBend[arg1]);
    ui->openGLWidget->currentRobot->setAngleTopRotation(topRotation[arg1]);

    ui->openGLWidget->update();

    setSliders();

}

void MainWindow::on_pushButton_stop_clicked()
{
   animationIsActive = false;
}

void MainWindow::on_pushButton_resume_clicked()
{
    animationIsActive = true;
}

void MainWindow::on_verticalScrollBar_animationTimeline_valueChanged(int value)
{
    ui->spinBox_oneFrameCurrent->setValue(value);
}

void MainWindow::on_actionOpen_animation_triggered()
{
    QString fileToOpen = QFileDialog::getOpenFileName(this,"Open file",QDir::currentPath());

    clearAnimation();

    QFile inputFile(fileToOpen);

    if(!inputFile.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", inputFile.errorString());
    }

    QTextStream inputStream(&inputFile);

    while(!inputStream.atEnd()) {

        QString line = inputStream.readLine();
        QStringList fields = line.split(",");

        baseRotation.push_back(fields[0].toInt());
        baseBend.push_back(fields[1].toInt());
        mediumRotation.push_back(fields[2].toInt());
        mediumBend.push_back(fields[3].toInt());

        topRotation.push_back(fields[4].toInt());
        topBend.push_back(fields[5].toInt());
        gripper.push_back(fields[6].toInt());

    }

    inputFile.close();

    ui->verticalScrollBar_animationTimeline->setEnabled(true);

    ui->spinBox_oneFrameCurrent->setEnabled(true);

    ui->verticalScrollBar_animationTimeline->setMaximum(baseRotation.size()-1);

    ui->spinBox_oneFrameCurrent->setMaximum(baseRotation.size()-1);

}

void MainWindow::on_actionFor_robot_triggered()
{
    QString savedFile = QFileDialog::getSaveFileName(this,"Save file",QDir::currentPath());



    QFile outputFile(savedFile);

    outputFile.open(QIODevice::WriteOnly);

    QTextStream outputStream( &outputFile );

    std::vector<float> tempBaseRotation(baseRotation);
    std::vector<float> tempBaseBend(baseBend);
    std::vector<float> tempMediumRotation(mediumRotation);
    std::vector<float> tempMediumBend(mediumBend);
    std::vector<float> tempTopRotation(topRotation);
    std::vector<float> tempTopBend(topBend);
    std::vector<float> tempGripper(gripper);

    smoothVector(tempBaseRotation);
    smoothVector(tempBaseBend);
    smoothVector(tempMediumRotation);
    smoothVector(tempMediumBend);
    smoothVector(tempTopRotation);
    smoothVector(tempTopBend);
    smoothVector(tempGripper);

    std::vector<std::vector<float> * > vectorPack;

    vectorPack.push_back(&tempBaseRotation);
    vectorPack.push_back(&tempBaseBend);
    vectorPack.push_back(&tempMediumRotation);
    vectorPack.push_back(&tempMediumBend);
    vectorPack.push_back(&tempTopRotation);
    vectorPack.push_back(&tempTopBend);
    vectorPack.push_back(&tempGripper);

    removeEqual(vectorPack);

    outputStream<<tempBaseRotation[0]<<','<< tempMediumRotation[0] << ',' << tempMediumRotation[0] << ',' << tempMediumBend[0] << ',' << tempTopRotation[0] << ',' << tempTopBend[0] << ',' << tempGripper[0]<< endl;

    for(unsigned int i = 0;i<tempBaseRotation.size();i++){

    outputStream<<tempBaseRotation[i]<<','<< tempMediumRotation[i] << ',' << tempMediumRotation[i] << ',' << tempMediumBend[i] << ',' << tempTopRotation[i] << ',' << tempTopBend[i] << ',' << tempGripper[i]<< endl;

    }

    outputStream<<'#';

    outputFile.close();
}

void MainWindow::on_actionRaw_version_triggered()
{

    QString savedFile = QFileDialog::getSaveFileName(this,"Save file",QDir::currentPath());



    QFile outputFile(savedFile);
    outputFile.open(QIODevice::WriteOnly);

    QTextStream outputStream( &outputFile );

    outputStream<<baseRotation[0]<<','<< baseBend[0] << ',' << mediumRotation[0] << ',' << mediumBend[0] << ',' << topRotation[0] << ',' << topBend[0] << ',' << gripper[0]<< endl;

    for(unsigned int i = 0;i<baseRotation.size();i++){

         outputStream<<baseRotation[i]<<','<< baseBend[i] << ',' << mediumRotation[i] << ',' << mediumBend[i] << ',' << topRotation[i] << ',' << topBend[i] << ',' << gripper[i]<< endl;

    }
    outputStream<<'#';

    outputFile.close();
}

void MainWindow::on_pushButton_new_clicked()
{
    on_actionNew_animation_triggered();
}

void MainWindow::on_actionNew_animation_triggered()
{
        animationIsActive = true;
        this->clearAnimation();

}
