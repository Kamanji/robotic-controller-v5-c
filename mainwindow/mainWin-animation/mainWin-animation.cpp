#include "mainwindow.h"


void MainWindow::clearAnimation(){

    baseRotation.clear();
    baseBend.clear();
    mediumBend.clear();
    mediumRotation.clear();
    topBend.clear();
    topRotation.clear();
    gripper.clear();

}

void MainWindow::writeToAnimation(){

    if(animationIsActive){

        ui->spinBox_oneFrameCurrent->setEnabled(true);
        ui->spinBox_oneFrameCurrent->setMaximum(baseRotation.size());

        ui->verticalScrollBar_animationTimeline->setMaximum(baseRotation.size());
        ui->verticalScrollBar_animationTimeline->setEnabled(true);


        baseRotation.push_back(ui->sliderBaseRotation->value());
        baseBend.push_back(ui->sliderBaseBend->value());
        mediumBend.push_back(ui->sliderMediumBend->value());


        mediumRotation.push_back(ui->sliderMediumRotation->value());
        topBend.push_back(ui->sliderTopBend->value());
        topRotation.push_back(ui->sliderTopRotation->value());
        gripper.push_back(ui->sliderGripper->value());




    }
}

void MainWindow::setSliders(){

    ui->sliderBaseBend->setValue(ui->openGLWidget->currentRobot->getAngleBaseBend());
    ui->sliderBaseRotation->setValue(ui->openGLWidget->currentRobot->getAngleBaseRotation());
    ui->sliderMediumBend->setValue(ui->openGLWidget->currentRobot->getAngleMediumBend());
    ui->sliderTopBend->setValue(ui->openGLWidget->currentRobot->getAngleTopBend());
    ui->sliderMediumRotation->setValue(ui->openGLWidget->currentRobot->getAngleMediumRotation());
    ui->sliderTopRotation->setValue(ui->openGLWidget->currentRobot->getAngleTopRotation());
    ui->sliderGripper->setValue(ui->openGLWidget->currentRobot->getAngleGripper());

}

