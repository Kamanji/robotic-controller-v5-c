#include "mainwindow.h"

void MainWindow::on_pushButton_deleteCurrentFrame_clicked(){

    animationIsActive = false;

    ui->spinBox_oneFrameCurrent->setValue(0);
    ui->spinBox_oneFrameCurrent->setDisabled(true);
    ui->verticalScrollBar_animationTimeline->setDisabled(true);


    clearAnimation();

}

void MainWindow::on_spinBox_oneFrameCurrent_valueChanged(int arg1){

    int i = arg1;

    animationIsActive = false;

    ui->openGLWidget->currentRobot->setAngleBaseRotation(baseRotation[i]);
    ui->openGLWidget->currentRobot->setAngleBaseBend(baseBend[i]);
    ui->openGLWidget->currentRobot->setAngleMediumBend(mediumBend[i]);
    ui->openGLWidget->currentRobot->setAngleGripper(gripper[i]);
    ui->openGLWidget->currentRobot->setAngleMediumRotation(mediumRotation[i]);
    ui->openGLWidget->currentRobot->setAngleTopBend(topBend[i]);
    ui->openGLWidget->currentRobot->setAngleTopRotation(topRotation[i]);

    ui->openGLWidget->update();
    setSliders();

}

void MainWindow::on_pushButton_stop_clicked()
{
   animationIsActive = false;
}

void MainWindow::on_pushButton_resume_clicked()
{
    animationIsActive = true;
}

void MainWindow::on_verticalScrollBar_animationTimeline_valueChanged(int value)
{
    ui->spinBox_oneFrameCurrent->setValue(value);
}


