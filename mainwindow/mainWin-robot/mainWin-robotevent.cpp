#include "mainwindow.h"

void MainWindow::on_sliderBaseRotation_valueChanged(int value)
{
    if(ui->groupBox_slidersControl->underMouse()){
        ui->openGLWidget->currentRobot->setAngleBaseRotation(value);
        ui->openGLWidget->update();
        this->setCoordinatesSpinBox();
        this->writeToAnimation();
    }


}

void MainWindow::on_sliderBaseBend_valueChanged(int value)
{
    if(ui->groupBox_slidersControl->underMouse()){
        ui->openGLWidget->currentRobot->setAngleBaseBend(value);
        ui->openGLWidget->update();
        this->setCoordinatesSpinBox();
        this->writeToAnimation();
    }

}

void MainWindow::on_sliderMediumBend_valueChanged(int value)
{
    if(ui->groupBox_slidersControl->underMouse()){
        ui->openGLWidget->currentRobot->setAngleMediumBend(value);
        ui->openGLWidget->update();
        this->setCoordinatesSpinBox();
        this->writeToAnimation();
    }

}

void MainWindow::on_sliderTopBend_valueChanged(int value)
{
    if(ui->groupBox_slidersControl->underMouse()){
        ui->openGLWidget->currentRobot->setAngleTopBend(value);
        ui->openGLWidget->update();
        this->setCoordinatesSpinBox();
        this->writeToAnimation();
    }

}

void MainWindow::on_sliderMediumRotation_valueChanged(int value)
{
    if(ui->groupBox_slidersControl->underMouse()){
        ui->openGLWidget->currentRobot->setAngleMediumRotation(value);
        ui->openGLWidget->update();
        this->setCoordinatesSpinBox();
        this->writeToAnimation();
    }

}

void MainWindow::on_sliderTopRotation_valueChanged(int value)
{
    if(ui->groupBox_slidersControl->underMouse()){
        ui->openGLWidget->currentRobot->setAngleTopRotation(value);
        ui->openGLWidget->update();
        this->setCoordinatesSpinBox();
        this->writeToAnimation();
    }

}

void MainWindow::on_sliderGripper_valueChanged(int value)
{
    if(ui->groupBox_slidersControl->underMouse()){
        ui->openGLWidget->currentRobot->setAngleGripper(value);
        ui->openGLWidget->update();
        this->setCoordinatesSpinBox();
        this->writeToAnimation();
    }

}
