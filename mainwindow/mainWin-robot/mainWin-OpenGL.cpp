#include "mainwindow.h"


void MainWindow::setCoordinatesSpinBox(){
    ui->spinBox_xPosition->setValue(ui->openGLWidget->currentRobot->getEndEffectorX());
    ui->spinBox_yPosition->setValue(ui->openGLWidget->currentRobot->getEndEffectorY());
    ui->spinBox_zPosition->setValue(-(ui->openGLWidget->currentRobot->getEndEffectorZ()));
}

void MainWindow::on_spinBox_yPosition_valueChanged(int arg1)
{
    if(!ui->groupBox_slidersControl->underMouse()){
        ui->openGLWidget->currentRobot->setEndEffectorY(arg1);
        ui->openGLWidget->currentRobot->updateAngles();
        ui->openGLWidget->update();
        this->setSliders();
        this->writeToAnimation();
    }

}

void MainWindow::on_spinBox_xPosition_valueChanged(int arg1)
{
    if(!ui->groupBox_slidersControl->underMouse()){

        ui->openGLWidget->currentRobot->setEndEffectorX(arg1);
        ui->openGLWidget->currentRobot->updateAngles();
        ui->openGLWidget->update();
        this->setSliders();
        this->writeToAnimation();
    }

}

void MainWindow::on_spinBox_zPosition_valueChanged(int arg1)
{
    if(!ui->groupBox_slidersControl->underMouse()){
        ui->openGLWidget->currentRobot->setEndEffectorZ(-arg1);
        ui->openGLWidget->currentRobot->updateAngles();
        ui->openGLWidget->update();
        this->setSliders();
        this->writeToAnimation();
    }

}

void MainWindow::on_action4dof_triggered()
{
    ui->inverseControlFrame->show();

    ui->openGLWidget->currentRobot = &ui->openGLWidget->robot4dof;

    currentMode = "4dof";

    ui->openGLWidget->update();

    ui->sliderTopBend->hide();
    ui->sliderTopRotation->hide();
    ui->sliderMediumRotation->hide();

    ui->spinBoxMediumRotation->hide();
    ui->spinBoxTopBend->hide();
    ui->spinBoxTopRotation->hide();

    this->applySettings();

    ui->openGLWidget->update();

}

void MainWindow::on_actionScara_triggered()
{
    ui->inverseControlFrame->hide();

    currentMode = "scara";

    ui->openGLWidget->currentRobot = &ui->openGLWidget->robotScara;

    ui->sliderTopBend->hide();
    ui->sliderTopRotation->hide();
    ui->sliderMediumRotation->hide();

    ui->spinBoxMediumRotation->hide();
    ui->spinBoxTopBend->hide();
    ui->spinBoxTopRotation->hide();

    this->applySettings();

    ui->openGLWidget->update();
}

void MainWindow::on_action6dof_triggered()
{
    ui->inverseControlFrame->show();

    currentMode = "6dof";

    ui->openGLWidget->currentRobot = &ui->openGLWidget->robot6dof;

    ui->openGLWidget->update();

    ui->sliderTopBend->show();
    ui->sliderMediumRotation->show();
    ui->sliderTopBend->show();
    ui->sliderTopRotation->show();

    ui->spinBoxMediumRotation->show();
    ui->spinBoxTopBend->show();
    ui->spinBoxTopRotation->show();

    this->applySettings();

    ui->openGLWidget->update();
}

